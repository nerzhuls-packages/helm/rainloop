[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/rainloop)](https://artifacthub.io/packages/search?repo=rainloop)

# rainloop

This helm chart is intended to deploy rainloop webmail with a persistent volume NFS for its data.

## Getting started

## Roadmap

Some parts can be made conditional and storage modified depending on the community needs